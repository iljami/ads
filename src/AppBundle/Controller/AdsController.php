<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ad;
use AppBundle\Entity\User;

use Doctrine\DBAL\Types\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType as TextType1;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdsController extends Controller
{
    /**
     * @Route("/", name="ads")
     */
    public function showAction(){

        return $this->render('ads/index.html.twig' );
    }

    /**
     * @Route("/ads/create", name="ads/create")
     */
    public function createAction(Request $request){

        $ad = new Ad;
        $user_id = $this->getUser()->getId();
        $username = $this->getUser()->getUsername();

        $form = $this->createFormBuilder($ad)
            ->add('title', TextType1::class, array('attr' => array('class' => 'form-control title-box', 'placeholder' => 'Enter Title')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control description-box')))
            ->add('submit', SubmitType::class, array('label' => 'Submit' ,'attr' => array('class' => 'btn btn-lg btn-primary btn-block submit-button')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $title = $form['title']->getData();
            $description = $form['description']->getData();
            $posted_at = new\DateTime('now');

            $ad->setTitle($title);
            $ad->setDescription($description);
            $ad->setPostedAt($posted_at);
            $ad->setUserId($user_id);
            $ad->setPostedBy($username);

            $em = $this->getDoctrine()->getManager();

            $em->persist($ad);
            $em->flush();

            $this->addFlash('notice', 'Advertisment Submited!');

            return $this->redirectToRoute('ads');
        }

        return $this->render('ads/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/ads/edit/{id}", name="ads/edit")
     */
    public function editAction($id, Request $request){

        $user_id = $this->getUser()->getId();

        $ad = $this->getDoctrine()->getRepository('AppBundle:Ad')->find($id);

        //checking if owner
        if($user_id != $ad->getUserId()){
            return $this->redirectToRoute('ads');
        }

        $posted_at = new\DateTime('now');

        $ad->setTitle($ad->getTitle());
        $ad->setDescription($ad->getDescription());
        $ad->setPostedAt($posted_at);


        $form = $this->createFormBuilder($ad)
            ->add('title', TextType1::class, array('attr' => array('class' => 'form-control title-box', 'placeholder' => 'Enter Title')))
            ->add('description', TextareaType::class, array('attr' => array('class' => 'form-control description-box')))
            ->add('submit', SubmitType::class, array('label' => 'Submit' ,'attr' => array('class' => 'btn btn-lg btn-primary btn-block submit-button')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $title = $form['title']->getData();
            $description = $form['description']->getData();
            $posted_at = new\DateTime('now');

            $em = $this->getDoctrine()->getManager();
            $ad = $em->getRepository('AppBundle:Ad')->find($id);

            $ad->setTitle($title);
            $ad->setDescription($description);
            $ad->setPostedAt($posted_at);
            $ad->setUserId($user_id);

            $em->flush();

            $this->addFlash('notice', 'Advertisment Edited!');

            return $this->redirectToRoute('ads');
        }

        return $this->render('ads/edit.html.twig', array(
            'ad' => $ad,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/ads/delete/{id}", name="ads/delete")
     */
    public function deleteAction($id){

        $current_user_id = $this->getUser()->getId();

        $em = $this->getDoctrine()->getManager();
        $ad = $em->getRepository('AppBundle:Ad')->find($id);

        //checking if owner
        if($current_user_id == $ad->getUserId()){
            $em->remove($ad);
            $em->flush();

            $this->addFlash('notice', 'Advertisment Deleted!');

            return $this->redirectToRoute('ads');

        } else if ($current_user_id != $ad->getUserId()){
            return $this->redirectToRoute('ads');
        }

    }

    /**
     * @Route("/ads/show", name="ads/show")
     */

    public function showUsersAdsAction(){
        $user_id = $this->getUser()->getId();

        $ads = $this->getDoctrine()->getRepository('AppBundle:Ad')->findByUserId($user_id);

        return $this->render('ads/users_ads.html.twig', array(
            'ads' => $ads
        ));
    }

    /**
     * @Route("/newest/ads", name="newest/ads")
     */

    public function newestAdsAction(){

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('
            SELECT a
            FROM AppBundle:Ad a
            ORDER BY a.postedAt DESC
        ');

        $ads = $query->getResult();

        return $this->render('ads/templates/newest_ads.html.twig', array(
            'ads' => $ads
        ));
    }


}
