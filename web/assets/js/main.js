$(document).ready(function(){

    // refresh page content function
    function refresh(){
        $('.update').load('/newest/ads');
    }

    // refreshes the page on load
    refresh();

    //refreshes the page content every 7 sec
    setInterval(function(){
        refresh();
    }, 7000);
});