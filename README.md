This is a small application which can be used to post advertisments.

It can be viewed in an online free hosting page: 

http://iljamiscenkovas.000webhostapp.com/


Deployment instructions:

1) Clone the project to a desired location where it could be reached by the server.

2) Create an empty database.

3) Configure database settings in ads/app/config/parameters.yml

4) Open terminal and navigate using "cd" to the project root "ads".

5) Enter the following command to create database tables "php bin/console doctrine:schema:update --force".

6) In the project root create ".htcaccess" file.

7) Add the following code to ".htcaccess" 
	
	RewriteEngine on
	RewriteCond %{HTTP_HOST} ^www.your.http.host.here$ [NC,OR]
	RewriteCond %{HTTP_HOST} ^www.your.http.host.here$
	RewriteCond %{REQUEST_URI} !web/
	RewriteRule (.*) /web/$1 [L]

8) Change "www.your.http.host.here" to your server name.

9) Start your server.

10) Application is good to go
